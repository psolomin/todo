# Todo

I am using this repo for experiments with Elixir / Erlang OTP components, such as:

- Exercises from "Elixir in Action" textbook
- GenStage lib
- `riak_core` framework
- Phoenix Plugs
- HTTP SSE endpoints
- `libcluster`
- ...

## Commands

- Install dependencies: `mix deps.get`
- Run formatter: `mix format`
- Run tests: `mix test`
- Project shell: `iex -S mix compile`
