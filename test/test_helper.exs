# (1)
Application.load(:todo)

# (2)
for app <- Application.spec(:todo, :applications) do
  Application.ensure_all_started(app)
end

Application.ensure_all_started(:mox)

ExUnit.start(exclude: [:skip])
