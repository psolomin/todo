defmodule Todo.ServerTest do
  use ExUnit.Case, async: false

  import Mox

  alias Todo.Server
  alias Todo.ProcessRegistry

  setup :set_mox_global
  setup :verify_on_exit!

  @list_name "my-todo"

  setup_all do
    {:ok, _pid} = ProcessRegistry.start_link()

    :ok
  end

  setup do
    Process.flag(:trap_exit, true)

    on_exit(fn ->
      ref = Process.monitor(pid())
      assert_receive {:DOWN, ^ref, _, _, _}
    end)

    :ok
  end

  @date {2019, 1, 1}
  @entry %{date: @date, title: "Dentist"}

  test "todo server is alive after starting" do
    Todo.DatabaseMock |> expect(:get, fn _ -> nil end)
    start_and_register(Todo.DatabaseMock)
    assert true == Process.alive?(pid())
  end

  test "todo server does not accumulate unknown messages" do
    Todo.DatabaseMock |> expect(:get, fn _ -> nil end)
    start_and_register(Todo.DatabaseMock)
    send(pid(), {:garbage, "garbage"})
    :timer.sleep(50)
    assert {:message_queue_len, 0} == Process.info(pid(), :message_queue_len)
  end

  test "an entry added to todo server can be retrieved" do
    Todo.DatabaseMock
    |> expect(:get, fn _ -> nil end)
    |> expect(:store, fn _, _ -> :ok end)

    start_and_register(Todo.DatabaseMock)
    Server.add_entry(@list_name, @entry)
    entries = Server.entries(@list_name, @date)
    assert [entry_with_id(1)] == entries
  end

  test "an entry added to todo server can be updated" do
    Todo.DatabaseMock
    |> expect(:get, fn _ -> nil end)
    |> expect(:store, 3, fn _, _ -> :ok end)

    start_and_register(Todo.DatabaseMock)
    Server.add_entry(@list_name, @entry)
    Server.add_entry(@list_name, @entry)
    Server.update_entry(@list_name, 2, fn e -> Map.put(e, :title, "Dentist 2") end)

    assert [
             %{date: @date, title: "Dentist", id: 1},
             %{date: @date, title: "Dentist 2", id: 2}
           ] == Server.entries(@list_name, @date)
  end

  test "an entry added to todo server can be deleted" do
    Todo.DatabaseMock
    |> expect(:get, fn _ -> nil end)
    |> expect(:store, 4, fn _, _ -> :ok end)

    start_and_register(Todo.DatabaseMock)
    Server.add_entry(@list_name, @entry)
    Server.add_entry(@list_name, @entry)
    assert 2 == length(Server.entries(@list_name, @date))
    Server.delete_entry(@list_name, 2)
    assert 1 == length(Server.entries(@list_name, @date))
    Server.delete_entry(@list_name, 1)
    assert 0 == length(Server.entries(@list_name, @date))
  end

  test "entries stored can be retrieved when server starts" do
    entries = [
      %{date: {2019, 1, 1}, title: "Dentist"},
      %{date: {2019, 1, 2}, title: "Pi ka bu"}
    ]

    list = for entry <- entries, into: TodoList.new(), do: entry

    Todo.DatabaseMock |> expect(:get, fn _ -> list end)

    start_and_register(Todo.DatabaseMock)

    assert [%{date: {2019, 1, 2}, title: "Pi ka bu", id: 2}] ==
             Server.entries(@list_name, {2019, 1, 2})
  end

  defp start_and_register(db) do
    {:ok, _pid} = Server.start_link(%{db_module: db, list_name: @list_name})
  end

  defp pid() do
    Server.whereis(@list_name)
  end

  defp entry_with_id(id) do
    Map.put(@entry, :id, id)
  end
end
