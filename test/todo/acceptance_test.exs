defmodule Todo.AcceptanceTest do
  use ExUnit.Case, async: false

  @date {2019, 1, 1}
  @entry %{date: @date, title: "Dentist"}
  @other_entry %{date: @date, title: "Dentist 2"}
  @db_dir "./test/resources/data/acceptance"
  @app_name :app_test
  @cache_name :acceptance_cache
  @todo_name "my-todo"

  setup do
    Process.flag(:trap_exit, true)
    File.rm_rf(@db_dir)

    on_exit(fn ->
      ref = Process.monitor(app_pid())
      assert_receive {:DOWN, ^ref, _, _, _}
      File.rm_rf!(@db_dir)
    end)

    :ok
  end

  test "item stored in some todo list can be retrieved after full restart of the app" do
    start_and_register()

    Todo.Cache.server_process(cache_pid(), @todo_name)
    Todo.Server.add_entry(@todo_name, @entry)
    Todo.Server.add_entry(@todo_name, @other_entry)
    assert 2 == length(Todo.Server.entries(@todo_name, @date))

    # needed because otherwise storage does not keep up with
    # by saving data to disk:
    :timer.sleep(100)

    # restart the app:
    assert :ok == Supervisor.stop(app_pid())
    start_and_register()

    # needed because Supervisor takes some time starting the process again
    :timer.sleep(50)

    Todo.Cache.server_process(cache_pid(), @todo_name)

    assert [
             entry_with_id(@entry, 1),
             entry_with_id(@other_entry, 2)
           ] == Todo.Server.entries(@todo_name, @date)
  end

  defp start_and_register() do
    {:ok, pid} = Todo.Supervisor.start_link(%{db_dir: @db_dir, cache_name: @cache_name})
    Process.register(pid, @app_name)
  end

  defp app_pid() do
    Process.whereis(@app_name)
  end

  defp cache_pid() do
    Process.whereis(@cache_name)
  end

  defp entry_with_id(entry, id) do
    Map.put(entry, :id, id)
  end
end
