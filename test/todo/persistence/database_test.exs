defmodule Todo.DatabaseTest do
  use ExUnit.Case, async: false

  alias Todo.ProcessRegistry
  alias Todo.Database

  @db_dir "./test/resources/data/database"

  setup_all do
    {:ok, _pid} = ProcessRegistry.start_link()

    :ok
  end

  setup do
    Process.flag(:trap_exit, true)
    File.rm_rf(@db_dir)
    start_and_register()

    on_exit(fn ->
      db_ref = Process.monitor(db_pid())
      assert_receive {:DOWN, ^db_ref, _, _, _}
      File.rm_rf(@db_dir)
    end)

    :ok
  end

  test "database server is alive after starting" do
    assert true == Process.alive?(db_pid())
  end

  test "database can store item and give it back" do
    Database.store("foo", {:bar, "bar"})
    assert {:bar, "bar"} == Database.get("foo")
  end

  test "database can store item and give it back after being restarted" do
    Database.store("foo", {:bar, "bar"})
    # needed because store() is async
    :timer.sleep(50)
    assert :ok == Supervisor.stop(db_pid())
    start_and_register()
    assert {:bar, "bar"} == Database.get("foo")
  end

  defp start_and_register() do
    {:ok, db_supervisor_pid} = Database.start_link(@db_dir)
    Process.register(db_supervisor_pid, :db_superv_test)
  end

  defp db_pid(), do: Process.whereis(:db_superv_test)
end
