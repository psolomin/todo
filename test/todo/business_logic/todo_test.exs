defmodule TodoTest do
  use ExUnit.Case

  import ExUnit.CaptureIO

  test "todo list entries can be retrieved" do
    l =
      TodoList.new()
      |> TodoList.add_entry(%{date: {2019, 1, 1}, title: "Dentist"})
      |> TodoList.add_entry(%{date: {2019, 1, 1}, title: "Haircut", notes: "Claudia"})
      |> TodoList.add_entry(%{date: {2019, 1, 2}, title: "Pi ka bu"})

    assert TodoList.entries(l, {2019, 1, 1}) == [
             %{date: {2019, 1, 1}, title: "Dentist", id: 1},
             %{date: {2019, 1, 1}, title: "Haircut", notes: "Claudia", id: 2}
           ]
  end

  test "todo list entries can be updated" do
    l =
      TodoList.new()
      |> TodoList.add_entry(%{date: {2019, 1, 2}, title: "Pi ka bu"})

    l_updated = TodoList.update_entry(l, 1, &Map.put(&1, :date, {2019, 1, 3}))

    assert TodoList.entries(l_updated, {2019, 1, 3}) == [
             %{date: {2019, 1, 3}, title: "Pi ka bu", id: 1}
           ]
  end

  test "non-existing entry can not be updated" do
    l =
      TodoList.new()
      |> TodoList.add_entry(%{date: {2019, 1, 2}, title: "Pi ka bu"})

    l_updated = TodoList.update_entry(l, 42, &Map.put(&1, :date, {2019, 1, 3}))
    assert TodoList.entries(l_updated, {2019, 1, 3}) == []
  end

  test "an existing entry can be deleted" do
    l =
      TodoList.new()
      |> TodoList.add_entry(%{date: {2019, 1, 2}, title: "Pi ka bu"})

    l_after_deletion = TodoList.delete_entry(l, 1)
    assert TodoList.entries(l, {2019, 1, 2}) == [%{id: 1, date: {2019, 1, 2}, title: "Pi ka bu"}]
    assert TodoList.entries(l_after_deletion, {2019, 1, 2}) == []
  end

  test "deletion of non-existing entry produces no effect" do
    l =
      TodoList.new()
      |> TodoList.add_entry(%{date: {2019, 1, 2}, title: "Pi ka bu"})

    l_after_deletion = TodoList.delete_entry(l, 42)
    assert TodoList.entries(l, {2019, 1, 2}) == [%{id: 1, date: {2019, 1, 2}, title: "Pi ka bu"}]

    assert TodoList.entries(l_after_deletion, {2019, 1, 2}) == [
             %{id: 1, date: {2019, 1, 2}, title: "Pi ka bu"}
           ]
  end

  test "a TodoList can be initialized from a list of entries" do
    l = TodoList.new([%{date: {2019, 1, 2}, title: "Pi ka bu"}])
    assert TodoList.entries(l, {2019, 1, 2}) == [%{id: 1, date: {2019, 1, 2}, title: "Pi ka bu"}]
  end

  test "a TodoList can be initialized from a file with entries" do
    l = TodoList.CsvImporter.import("./test/resources/todo_list.csv")

    assert TodoList.entries(l, {2013, 12, 19}) == [
             %{id: 1, date: {2013, 12, 19}, title: "Dentist"},
             %{id: 3, date: {2013, 12, 19}, title: "Movies"}
           ]
  end

  test "a TodoList can be printed to stdout" do
    fun = fn -> assert IO.puts(TodoList.new()) == :ok end
    assert capture_io(fun) == "#TodoList\n"
  end

  test "a TodoList can be built using `into` function of Collectable protocol" do
    entries = [
      %{date: {2019, 1, 1}, title: "Dentist"},
      %{date: {2019, 1, 2}, title: "Pi ka bu"}
    ]

    list = for entry <- entries, into: TodoList.new(), do: entry

    assert list ==
             TodoList.new()
             |> TodoList.add_entry(%{date: {2019, 1, 1}, title: "Dentist"})
             |> TodoList.add_entry(%{date: {2019, 1, 2}, title: "Pi ka bu"})
  end
end
