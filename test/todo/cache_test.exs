defmodule Todo.CacheTest do
  use ExUnit.Case, async: false

  import Mox

  alias Todo.Cache
  alias Todo.Server
  alias Todo.ProcessRegistry
  alias Todo.ServerSupervisor

  setup :set_mox_global
  setup :verify_on_exit!

  @date {2019, 1, 1}
  @entry %{date: @date, title: "Dentist"}
  @other_entry %{date: @date, title: "Dentist"}

  setup_all do
    {:ok, _pid} = ProcessRegistry.start_link()
    {:ok, _pid} = ServerSupervisor.start_link()

    :ok
  end

  setup do
    Process.flag(:trap_exit, true)

    on_exit(fn ->
      ref = Process.monitor(pid())
      assert_receive {:DOWN, ^ref, _, _, _}
    end)

    :ok
  end

  test "todo cache is alive after starting" do
    start_and_register(Todo.DatabaseMock)
    assert true == Process.alive?(pid())
  end

  test "todo cache does not accumulate unknown messages" do
    start_and_register(Todo.DatabaseMock)
    send(pid(), {:garbage, "garbage"})
    :timer.sleep(50)
    assert {:message_queue_len, 0} == Process.info(pid(), :message_queue_len)
  end

  test "todo cache gives specific todo server with corresponding state" do
    Todo.DatabaseMock
    |> expect(:store, 2, fn _, _ -> :ok end)
    |> expect(:get, 2, fn _ -> nil end)

    start_and_register(Todo.DatabaseMock)

    Cache.server_process(pid(), "mary's todo")
    Cache.server_process(pid(), "john's todo")
    Server.add_entry("mary's todo", @entry)
    Server.add_entry("john's todo", @other_entry)
    Cache.server_process(pid(), "john's todo")
    assert [entry_with_id(@entry, 1)] == Server.entries("mary's todo", @date)
    assert [entry_with_id(@other_entry, 1)] == Server.entries("john's todo", @date)
  end

  defp start_and_register(db) do
    {:ok, pid} = Cache.start_link(%{db_module: db})
    Process.register(pid, :todo_cache_test)
  end

  defp pid() do
    Process.whereis(:todo_cache_test)
  end

  defp entry_with_id(entry, id) do
    Map.put(entry, :id, id)
  end
end
