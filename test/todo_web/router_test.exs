defmodule Example.RouterTest do
  use ExUnit.Case
  use Plug.Test

  import Mock

  alias TodoWeb.Router

  @opts Router.init([])
  @app_name :app_integration

  test "returns Home" do
    conn = :get |> conn("/", "") |> Router.call(@opts)
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "Home"
  end

  @tag :skip
  test "returns Entries for a given list and given date" do
    query = "list=bob&date=2013-12-19"

    with_mocks([
      {Todo.Server, [], [entries: fn _, _ -> "Whatever" end]}
    ]) do
      conn = :get |> conn("/entries?#{query}") |> Router.call(@opts)
      assert conn.state == :sent
      assert conn.status == 200
      # TODO: eliminate hard-coded implementation
      assert conn.resp_body == "Dentist; Dentist"
    end
  end

  @tag :skip
  test "adding an entry returns 201" do
    query = "list=bob&date=2013-12-19&title=Dentist"

    with_mocks([
      {Todo.Server, [], [add_entry: fn _, _ -> :ok end]}
    ]) do
      conn = :post |> conn("/entries?#{query}", "") |> Router.call(@opts)
      assert conn.state == :sent
      assert conn.status == 201
    end
  end

  test "returns 404" do
    conn = :get |> conn("/nothing-here", "") |> Router.call(@opts)
    assert conn.state == :sent
    assert conn.status == 404
  end

  defp start_and_register() do
    {:ok, pid} = Todo.start(nil, nil)
    Process.register(pid, @app_name)
  end

  defp app_pid() do
    Process.whereis(@app_name)
  end
end
