defmodule KeyValueStoreServerTest do
  use ExUnit.Case

  test "key-value store server starts" do
    {:ok, pid} = KeyValueStore.start()
    assert true == Process.alive?(pid)
  end

  test "key-value stores items and retrieves them" do
    {:ok, pid} = KeyValueStore.start()
    KeyValueStore.put(pid, :foo, "bar")
    assert "bar" == KeyValueStore.get(pid, :foo)
    assert true == Process.exit(pid, {self(), :normal})
  end
end
