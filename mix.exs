defmodule Todo.MixProject do
  use Mix.Project

  def project do
    [
      app: :todo,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      applications: [:gproc, :cowboy, :plug],
      extra_applications: [:logger],
      mod: {Todo, []},
      env: [
        port: 8080
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:gproc, "~> 0.8.0"},
      {:plug, "~> 1.8.3"},
      {:plug_cowboy, "~> 2.1.0"},
      {:mox, "~> 0.4", only: :test},
      {:mock, "~> 0.3.3", only: :test},
      {:httpoison, "~> 1.5.1", only: :test},
      {:gen_stage, "~> 1.0"}
    ]
  end

  defp elixirc_paths(:test), do: ["test/support", "lib"]
  defp elixirc_paths(_), do: ["lib"]
  defp aliases, do: [test: "test --no-start"]
end
