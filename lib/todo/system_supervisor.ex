defmodule Todo.SystemSupervisor do
  use Supervisor

  @db_module Todo.Database
  @cache_module Todo.Cache
  @todo_server_supervisor_module Todo.ServerSupervisor
  @db_dir "lib/resources/data"
  @cache_name :cache

  def start_link(args \\ %{}) do
    Supervisor.start_link(__MODULE__, args)
  end

  def init(args) do
    db_dir = args[:db_dir] || @db_dir
    cache_name = args[:cache_name] || @cache_name

    children = [
      supervisor(@db_module, [db_dir]),
      supervisor(@todo_server_supervisor_module, []),
      worker(@cache_module, [%{db_module: @db_module, cache_name: cache_name}])
    ]

    supervise(children, strategy: :one_for_one)
  end
end
