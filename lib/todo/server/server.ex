defmodule Todo.Server do
  use GenServer

  def start_link(args) do
    %{db_module: _db_module, list_name: list_name} = args
    GenServer.start_link(Todo.Server, args, name: via_tuple(list_name))
  end

  def init(args) do
    send(self(), :try_todo_list_from_db)
    init_state = Map.put(args, :todo_list, nil)
    {:ok, init_state}
  end

  def whereis(name), do: :gproc.whereis_name({:n, :l, {:todo_server, name}})

  def add_entry(list_name, new_entry) do
    GenServer.cast(via_tuple(list_name), {:add_entry, new_entry})
  end

  def update_entry(list_name, entry_id, updater_fun) do
    GenServer.cast(via_tuple(list_name), {:update_entry, entry_id, updater_fun})
  end

  def delete_entry(list_name, entry_id) do
    GenServer.cast(via_tuple(list_name), {:delete_entry, entry_id})
  end

  def entries(list_name, date) do
    GenServer.call(via_tuple(list_name), {:entries, date})
  end

  def handle_call({:entries, date}, _request_id, %{todo_list: todo_list} = state) do
    {:reply, TodoList.entries(todo_list, date), state}
  end

  def handle_cast({:add_entry, new_entry}, state) do
    %{db_module: db_module, list_name: list_name, todo_list: todo_list} = state
    new_list = TodoList.add_entry(todo_list, new_entry)
    :ok = db_module.store(list_name, new_list)
    {:noreply, %{state | todo_list: new_list}}
  end

  def handle_cast({:update_entry, entry_id, updater_fun}, state) do
    %{db_module: db_module, list_name: list_name, todo_list: todo_list} = state
    new_list = TodoList.update_entry(todo_list, entry_id, updater_fun)
    :ok = db_module.store(list_name, new_list)
    {:noreply, %{state | todo_list: new_list}}
  end

  def handle_cast({:delete_entry, entry_id}, state) do
    %{db_module: db_module, list_name: list_name, todo_list: todo_list} = state
    new_list = TodoList.delete_entry(todo_list, entry_id)
    :ok = db_module.store(list_name, new_list)
    {:noreply, %{state | todo_list: new_list}}
  end

  def handle_info(:try_todo_list_from_db, state) do
    %{db_module: db_module, list_name: list_name} = state
    init_todo_list = db_module.get(list_name) || TodoList.new()
    {:noreply, %{state | todo_list: init_todo_list}}
  end

  def handle_info(_other, state) do
    {:noreply, state}
  end

  defp via_tuple(name), do: {:via, :gproc, {:n, :l, {:todo_server, name}}}
end
