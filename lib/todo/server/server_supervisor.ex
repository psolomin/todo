defmodule Todo.ServerSupervisor do
  use DynamicSupervisor

  def start_link() do
    DynamicSupervisor.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_child(db_module, todo_list_name) do
    DynamicSupervisor.start_child(
      __MODULE__,
      {Todo.Server, %{db_module: db_module, list_name: todo_list_name}}
    )
  end
end
