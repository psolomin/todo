defmodule Todo.Cache do
  use GenServer

  alias Todo.Server

  def start_link(args) do
    case args[:cache_name] do
      nil -> GenServer.start_link(__MODULE__, args)
      _ -> GenServer.start_link(__MODULE__, args, name: validate_name(args[:cache_name]))
    end
  end

  def server_process(cache_pid, todo_list_name) do
    case Server.whereis(todo_list_name) do
      :undefined -> GenServer.call(cache_pid, {:server_process, todo_list_name})
      other -> other
    end
  end

  def init(%{db_module: db_module} = _args) do
    {:ok, %{db_module: db_module}}
  end

  def handle_call({:server_process, todo_list_name}, _, %{db_module: db_module} = state) do
    case Server.whereis(todo_list_name) do
      :undefined ->
        {:ok, new_server} = Todo.ServerSupervisor.start_child(db_module, todo_list_name)
        {:reply, new_server, state}

      other ->
        {:reply, other, state}
    end
  end

  def handle_info(_other, state) do
    {:noreply, state}
  end

  defp validate_name(name) when is_atom(name), do: name
  defp validate_name(_), do: raise("Invalid initialization input.")
end
