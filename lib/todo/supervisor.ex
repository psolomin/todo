defmodule Todo.Supervisor do
  use Supervisor

  @todo_system_supervisor_module Todo.SystemSupervisor

  def start_link(args \\ %{}) do
    Supervisor.start_link(__MODULE__, args)
  end

  def init(args) do
    children = [
      supervisor(@todo_system_supervisor_module, [args])
    ]

    supervise(children, strategy: :rest_for_one)
  end
end
