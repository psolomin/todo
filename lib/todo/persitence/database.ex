defmodule Todo.Database.Behaviour do
  @callback start_link(String.t()) :: {:error, String.t()} | {:ok, pid()}
  @callback store(any(), any()) :: {:error, String.t()} | {:noreply, any()}
  @callback get(any()) :: {:error, String.t()} | {:ok, any()}
end

defmodule Todo.Database do
  @behaviour Todo.Database.Behaviour

  alias Todo.PoolSupervisor
  alias Todo.DatabaseWorker

  @n_workers 3

  @spec start_link(String.t()) :: {:error, String.t()} | {:ok, pid}
  def start_link(db_folder) do
    PoolSupervisor.start_link(db_folder, @n_workers)
  end

  @spec store(any(), any()) :: {:error, String.t()} | :ok
  def store(key, data) do
    key |> choose_worker |> DatabaseWorker.store(key, data)
  end

  @spec get(any()) :: {:error, String.t()} | any()
  def get(key) do
    key |> choose_worker |> DatabaseWorker.get(key)
  end

  defp choose_worker(key) do
    :erlang.phash2(key, @n_workers) + 1
  end
end
