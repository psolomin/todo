defmodule TodoWeb.Router do
  use Plug.Router
  use Plug.ErrorHandler

  plug(Plug.Logger)
  plug(:match)
  plug(:dispatch)

  def start_link() do
    Plug.Cowboy.http(Plugger.Router, [])
  end

  get("/", do: conn |> send_resp(200, "Home"))
  get("/entries", do: conn |> fetch_params |> entries |> get_respond)
  post("/entries", do: conn |> fetch_params |> add_entry |> post_respond)
  match(_, do: send_resp(conn, 404, ""))

  defp fetch_params(conn) do
    Plug.Conn.fetch_query_params(conn)
  end

  defp add_entry(conn) do
    _list_pid = conn |> get_or_create_todo
    list_name = conn |> extract_list_name
    entry = conn |> params_to_entry
    Todo.Server.add_entry(list_name, entry)
    Plug.Conn.assign(conn, :response, "OK")
  end

  defp entries(conn) do
    _list_pid = conn |> get_or_create_todo
    list_name = conn |> extract_list_name
    date = conn |> get_date
    entries = list_name |> Todo.Server.entries(date) |> entries_to_payload
    Plug.Conn.assign(conn, :response, entries)
  end

  defp handle_errors(conn, %{kind: kind, reason: reason, stack: stack}) do
    IO.inspect(kind, label: :kind)
    IO.inspect(reason, label: :reason)
    IO.inspect(stack, label: :stack)
    send_resp(conn, conn.status, "Something went wrong")
  end

  defp extract_list_name(conn) do
    conn.params["list"]
  end

  defp params_to_entry(conn) do
    %{date: parse_date(conn.params["date"]), title: conn.params["title"]}
  end

  defp get_respond(conn) do
    conn
    |> Plug.Conn.put_resp_content_type("text/plain")
    |> Plug.Conn.send_resp(200, conn.assigns[:response])
  end

  defp post_respond(conn) do
    conn
    |> Plug.Conn.put_resp_content_type("text/plain")
    |> Plug.Conn.send_resp(201, conn.assigns[:response])
  end

  defp get_date(conn) do
    parse_date(conn.params["date"])
  end

  defp parse_date(date_text) do
    List.to_tuple(Enum.map(String.split(date_text, "-"), &String.to_integer(&1)))
  end

  defp call_processes_cache(list_name) do
    Todo.Cache.server_process(:cache, list_name)
  end

  defp get_or_create_todo(conn) do
    conn |> extract_list_name |> call_processes_cache
  end

  defp entries_to_payload(_entries) do
    # TODO: replace with real implementation
    "Dentist; Dentist"
  end
end
