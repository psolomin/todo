defmodule TodoWeb do
  use Application

  def start(_type, _args) do
    case Application.get_env(:todo, :port) do
      nil ->
        raise("Port not specified!")

      port ->
        children = [
          Plug.Cowboy.child_spec(scheme: :http, plug: TodoWeb.Router, options: [port: port])
        ]

        Supervisor.start_link(children, strategy: :one_for_one)
    end
  end
end
