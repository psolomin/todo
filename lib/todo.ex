defmodule Todo do
  use Application

  def start(_type, _args) do
    children = [
      {Todo.Supervisor, []}
    ]

    res = Supervisor.start_link(children, strategy: :one_for_one)
    TodoWeb.start(nil, nil)
    res
  end
end
