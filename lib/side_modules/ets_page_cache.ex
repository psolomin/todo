defmodule EtsPageCache do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, nil, name: :ets_page_cache)
  end

  def init(_) do
    :ets.new(:ets_page_cache, [:set, :named_table, :protected])
    {:ok, nil}
  end

  def cached(key, lambda) do
    read_cached(key) || GenServer.call(:ets_page_cache, {:cached, key, lambda})
  end

  def handle_call({:cached, key, lambda}, _request_id, state) do
    res = read_cached(key) || cache_response(key, lambda)
    {:reply, res, state}
  end

  defp read_cached(key) do
    case :ets.lookup(:ets_page_cache, key) do
      [{^key, cached}] -> cached
      _ -> nil
    end
  end

  defp cache_response(key, lambda) do
    response = lambda.()
    :ets.insert(:ets_page_cache, {key, response})
    response
  end
end

defmodule WebServer do
  def index() do
    :timer.sleep(100)
    "<html>...</html>"
  end
end
