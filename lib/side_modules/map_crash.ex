defmodule MapCrash do
  def start(fuel \\ 5) do
    do_loop(%{}, fuel)
  end

  defp do_loop(state, fuel) do
    case fuel do
      0 ->
        throw("Finished!")
        nil

      _ ->
        nil
    end

    # key = String.to_atom("i-am-atom-#{fuel}")
    key = "i-am-atom-#{fuel}"
    val = "foo"
    do_loop(Map.put(state, key, val), fuel - 1)
  end
end
