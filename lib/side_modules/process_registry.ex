defmodule Todo.ProcessRegistry do
  import Kernel, except: [send: 2]

  use GenServer

  @name :reg
  @ets_name :ets_reg

  def start_link() do
    GenServer.start_link(__MODULE__, nil, name: @name)
  end

  def init(_) do
    do_init()
    {:ok, nil}
  end

  def register_name(key, pid) do
    case read_cached(key) do
      nil -> GenServer.call(@name, {:register_name, key, pid})
      _ -> :no
    end
  end

  def whereis_name(key) do
    read_cached(key) || :undefined
  end

  def unregister_name(key) do
    case read_cached(key) do
      nil -> :yes
      _ -> GenServer.call(@name, {:unregister_name, key})
    end
  end

  def send(key, message) do
    case whereis_name(key) do
      :undefined ->
        {:badarg, {key, message}}

      pid ->
        Kernel.send(pid, message)
        pid
    end
  end

  def handle_call({:register_name, key, pid}, _request_id, state) do
    case read_cached(key) do
      nil ->
        cache_record(key, pid)
        Process.monitor(pid)
        {:reply, :yes, state}

      _ ->
        {:reply, :no, state}
    end
  end

  def handle_call({:unregister_name, key}, _request_id, state) do
    delete_cached(key)
    {:reply, :yes, state}
  end

  def handle_info({:DOWN, _, :process, from_pid, _}, state) do
    deregister_pid(from_pid)
    {:noreply, state}
  end

  defp do_init() do
    :ets.new(@ets_name, [:set, :named_table, :protected])
  end

  defp deregister_pid(pid) do
    :ets.match_delete(@ets_name, {:_, pid})
  end

  defp cache_record(key, pid) do
    :ets.insert(@ets_name, {key, pid})
  end

  defp read_cached(key) do
    case :ets.lookup(@ets_name, key) do
      [{^key, cached}] -> cached
      _ -> nil
    end
  end

  defp delete_cached(key) do
    :ets.delete(@ets_name, key)
  end
end
