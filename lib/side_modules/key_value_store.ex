defmodule KeyValueStore do
  use GenServer

  def start() do
    GenServer.start(KeyValueStore, nil)
  end

  def put(pid, key, value) do
    GenServer.cast(pid, {:put, key, value})
  end

  def get(pid, key) do
    GenServer.call(pid, {:get, key})
  end

  def init(_) do
    # TODO: read more about how this `send_interval` works
    :timer.send_interval(5000, :cleanup)
    {:ok, Map.new()}
  end

  def handle_call({:get, key}, _request_id, state) do
    {:reply, Map.get(state, key), state}
  end

  def handle_cast({:put, key, value}, state) do
    {:noreply, Map.put(state, key, value)}
  end

  def handle_info(:cleanup, state) do
    IO.puts("Performing clean up...")
    {:noreply, state}
  end

  def handle_info(_other, state) do
    {:noreply, state}
  end
end
