defmodule A do
  use GenStage

  def start_link(number) do
    GenStage.start_link(A, number, name: :prod)
  end

  def init(counter) do
    {:producer, counter}
  end

  def handle_demand(demand, counter) when demand > 0 do
    IO.inspect("A: getting demand - #{demand}. Counter - #{counter}")
    # If the counter is 3 and we ask for 2 items, we will
    # emit the items 3 and 4, and set the state to 5.
    events = Enum.to_list(counter..(counter + demand - 1))
    IO.inspect("A: sending supply:")
    IO.inspect(events, charlists: :as_lists)
    {:noreply, events, counter + demand}
  end
end
