defmodule B do
  use GenStage

  def start_link(multiplier) do
    GenStage.start_link(B, multiplier, name: :prod_cons)
  end

  def init(multiplier) do
    {:producer_consumer, multiplier, subscribe_to: [{:prod, min_demand: 5, max_demand: 10}]}
  end

  def handle_events(events, _from, multiplier) do
    IO.inspect("B: Raw events:")
    IO.inspect("B: Length - #{length(events)}")
    IO.inspect(events, charlists: :as_lists)
    processed_events = Enum.map(events, &(&1 * multiplier))
    IO.inspect("B: Processed events:")
    IO.inspect(processed_events, charlists: :as_lists)
    {:noreply, events, multiplier}
  end

  def handle_info(:info, state) do
    IO.inspect("Got :info")
    {:noreply, [:lol], state}
  end
end
