defmodule C do
  use GenStage

  def start_link() do
    GenStage.start_link(C, %{demand: 3}, name: :cons)
  end

  def init(%{demand: demand} = _args) do
    {:consumer, :dummy, subscribe_to: [{:prod_cons, min_demand: 1, max_demand: demand}]}
  end

  def handle_events([:lol], _from, state) do
    IO.inspect("C: Received :lol")
    {:noreply, [], state}
  end

  def handle_events(event, _from, state) do
    # Wait for a second.
    IO.inspect("C: Received events. Length: #{length(event)}")

    Process.sleep(7000)

    # Inspect the events.
    IO.inspect("C: processed events:")
    IO.inspect(event, charlists: :as_lists)

    # We are a consumer, so we would never emit items.
    {:noreply, [], state}
  end
end
